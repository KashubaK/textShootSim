import Component from '@ember/component';

export default Component.extend({
    train: Ember.inject.service(),

    dummies: [],
    classNames: ["row"],

    actions: {
        fightDummy(idx) {
            this.get('train').train(idx);
        }
    }
});
