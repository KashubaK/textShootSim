import Component from '@ember/component';

export default Component.extend({
    user: Ember.inject.service(),

    signIn: true,
    message: "Sign into your account",
    error: false,

    dontFireVerification: false,

    email: "",
    emailObserver: Ember.observer('email', (self) => {
        if (self.get('signIn')) return;

        var email = self.get('email');

        if (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test
            (email) === false) {
                self.set('error', true);
                self.set('message', "Please enter a valid email address.");
            } else {
                if (!self.get('dontFireVerification')) {
                    self.handleInputVerification(self);
                }
            }
    }),

    display: "",
    displayObserver: Ember.observer('display', (self) => {
        var display = self.get('display');

        if (display.length > 20) {
            self.set('error', true);
            self.set('message', "Display name can't be more than 20 characters.");
        } else {
            if (!self.get('dontFireVerification')) {
                self.handleInputVerification(self);
            }
        }
    }),
    
    password: "",
    passwordObserver: Ember.observer('password', (self) => {
        if (self.get('signIn')) return;
        var pw = self.get('password');

        if (pw.length < 8) {
            self.set('error', true);
            self.set('message', "Password must be at least eight characters.");
        } else {
            if (self.get('signIn') == false) {
                if (!self.get('dontFireVerification')) {
                    self.handleInputVerification(self);
                }
            }
        }
    }),

    confirmPassword: "",
    confirmPasswordObserver: Ember.observer('confirmPassword', (self) => {
        var pw = self.get('confirmPassword');
        console.log(pw, self.get('password'))

        if (pw != self.get('password')) {
            self.set('error', true);
            self.set('message', 'Passwords must match.');
        } else {
            if (!self.get('dontFireVerification')) {
                self.handleInputVerification(self);
            }
        }
    }),

    handleInputVerification(self) {
        self.set('dontFireVerification', true);

        self.set('error', false);
        self.set('message', 'Create an account');

        self.emailObserver(self);
        self.displayObserver(self);
        self.passwordObserver(self);
        self.confirmPasswordObserver(self);

        self.set('dontFireVerification', false);
        
    },

    actions: {
        toggleSignIn() {
            this.set('signIn', !this.get('signIn'));

            if (this.get('signIn')) {
                this.set('message', "Sign into your account");
            } else {
                this.set('message', "Create an account");
            }
        },

        signIn() {
            this.set('error', false);
            
            this.get('user').signIn(this.get('email'), this.get('password')).then((user) => {
                this.set('message', `Welcome, ${user.display_name}!`);
                this.sendAction('dismissSignInModal');
            }, (error) => {
                this.set('error', true);
                this.set('message', error.responseJSON.message);
            })
        },

        signUp() {

        }
    }
});
