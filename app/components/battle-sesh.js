import Component from '@ember/component';

export default Component.extend({
    battleReport: [],
    currentActionIndex: 0,

    actions: {
        battle() {
            this.set('battleReport', []);
            this.set('currentActionIndex', 0);

            $.ajax({
                url: "http://208.53.120.129:8080/api/dummy"
            }).done((body) => {
                this.set('reporting', setInterval(() => {
                    var i = this.get('currentActionIndex');

                    var currentStatus = body[i];

                    this.set('currentStatus', currentStatus);
                    this.get('battleReport').pushObject(currentStatus.action);

                    if (i + 1 != body.length) {
                        this.set('currentActionIndex', i + 1);
                    } else {
                        clearInterval(this.get('reporting'));
                    }
                }, 1000));
            })
        }
    }
});
