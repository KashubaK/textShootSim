import Component from '@ember/component';

export default Component.extend({
    user: Ember.inject.service(),

    actions: {
        goToTrain() {
            this.sendAction("goToTrain");
        }
    }
});
