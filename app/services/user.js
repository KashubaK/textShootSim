import Service from '@ember/service';

export default Service.extend({
    token: "",
    user: {},

    renewToken() {
        return new Promise((resolve, reject) => {
            var lastToken = localStorage.getItem('token');
            
            if (lastToken) {
                $.ajax({
                    url: "http://localhost:8080/api/user/token",
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify({
                        token: lastToken
                    })
                }).done((user) => {
                    localStorage.setItem('token', user.token);
                    this.set('token', user.token);
                    
                    user.weapon = JSON.parse(user.weapon);
                    user.body = JSON.parse(user.body);
                    
                    this.set('user', user);

                    resolve(user.token);
                }).fail((body) => {
                    this.set('showSignInModal', true);

                    reject();
                })
            } else {
                this.set('showSignInModal', true);
                reject();
            }
        })
    },

    signIn(email, password) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: "http://localhost:8080/api/user/auth",
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
    
                data: JSON.stringify({
                    email: email,
                    password: password
                })
            }).done((body) => {
                localStorage.setItem('token', body.token);
                this.set('token', body.token);
    
                body.user.weapon = JSON.parse(body.user.weapon);
                this.set('user', body);
                resolve(body);
            }).fail((body) => {
                reject(body);
            })
        })
    },

    signUp(email, display_name, password) {
        $.ajax({
            url: "http://localhost:8080/api/user",
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            
            data: JSON.stringify({
                email: email,
                display_name: display_name,
                password: password
            })
        }).done((body) => {
            localStorage.setItem('token', body.token);
            this.set('token', body.token);

            this.set('user', body);
        }).fail((body) => {
            this.set('showSignInModal', true);
        })
    }
});