import Service from '@ember/service';

export default Service.extend({
    user: Ember.inject.service(),

    dummies: [],
    report: [],

    train(idx) {
        var self = this;
        var ws = this.get('ws');

        ws.send(JSON.stringify({
            type: 'train', 
            player: this.get('user').user, 
            idx: idx
        }));
        
        ws.onmessage = function(message) {
            var data = JSON.parse(message.data);

            if (data.type === 'newReport') {
                self.get('report').pushObject(data.report);
                console.log("report", data.report);
            }
        };
    },

    getDummies() {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: "http://localhost:8080/api/dummies"
            }).done((dummies) => {
                this.set('dummies', dummies);

                resolve(this.get('dummies'));
            }).fail((error) => {
                reject(error);
            })
        });
    },
    init() {
        this._super();

        const ws = new WebSocket('ws://localhost:8080/wss');

        this.set('ws', ws);
    }
});
