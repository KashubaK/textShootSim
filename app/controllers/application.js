import Controller from '@ember/controller';

export default Controller.extend({
    user: Ember.inject.service(),
    showSignInModal: false,

    actions: {
        goToTrain() {
            this.transitionToRoute('train');
        },

        dismissSignInModal() {
            this.set('showSignInModal', false);
        }
    },

    init() {
        this._super();

        this.get('user').renewToken().then(() => {
            this.set('showSignInModal', false);
        }, () => {
            this.set('showSignInModal', true);
        })
    }
});
