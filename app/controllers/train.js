import Controller from '@ember/controller';

export default Controller.extend({
    train: Ember.inject.service(),
    dummies: [],

    init() {
        this._super();

        this.get('train').getDummies().then((dummies) => {
            this.set('dummies', dummies);
        }, (error) => {
            throw error;
        })
    }
});
