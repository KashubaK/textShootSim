var fs = require('fs');
var mysql = require('mysql');

var app = require('express')();
var json = require('body-parser').json();
var cors = require('cors');

var generateDummyPlayer = require('./scripts/generateDummyPlayer');
var Battle = require('./class/Battle');

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'kashubak',
    password : 'I love the future 2017.',
    database : 'textshootsim'
});

app.use(json);

app.use(cors());

app.get('/api/dummy', (req, res) => {
    var battle = new Battle();

    battle.players = [
        generateDummyPlayer("jarRoD"),
        generateDummyPlayer("beTerr"),
    ];

    battle.calculateBattle(res);
});

const WebSocket = require('ws');

var server = require('http').createServer(app);
var port = process.env.port || 8080;

var listeners = [];

// Dynamically load routes
fs.readdir('./routes', (err, files) => {
    files.forEach((file) => {
        var route = require('./routes/' + file);

        app.use(`/api/${route.endpoint}`, route.generateRouter(connection));

        if (route.webSocketListeners) {
            listeners = listeners.concat(route.webSocketListeners);
        }
    });
    
    var wss = new WebSocket.Server({server: server, path: '/wss'});

    console.log('listeners', listeners);
    wss.on('connection', (ws) => {
        ws.on("message", function(message) {
            var message = JSON.parse(message);

            listeners.forEach(function(listener) {
                if (message.type === listener.type) {
                    console.log('handling listener', listener.type)
                    listener.handler(message, ws);
                }
            });
        });
    });

    server.listen(port, () => {
        console.log(`EfT text-based shooter simulation MMORPG started on port ${port} on ${Date()}`);
    });
});