var express = require('express');
var fs = require('fs');
var Battle = require('../class/Battle');

var dummies = [];

module.exports = {
    endpoint: 'dummies',
    generateRouter: function generateRouter(db) {
        var router = express.Router();
        
        router.get('/', (req, res) => {
            fs.readFile(__dirname + '/../dummies.json', "utf8", (err, dummies) => {
                if (err) return res.status(502).json(err);

                dummies = JSON.parse(dummies);
                console.log('dummies set', dummies)
                res.json(dummies);
            });
        });
    
        return router;
    },

    webSocketListeners: [
        {
            "type": "train",
            "handler": function(data, ws) {
                fs.readFile(__dirname + '/../dummies.json', "utf8", (err, dummies) => {
                    if (err) return res.status(502).json(err);
    
                    dummies = JSON.parse(dummies);

                    var player = data.player;
                    var target = dummies[data.idx];

                    var battle = new Battle();

                    battle.players = [player, target];

                    // TODO: Make calculateBattle return an array
                    var battle = battle.calculateBattle();
                    var current = 0;

                    var interval = setInterval(function() {
                        ws.send(JSON.stringify({
                            type: "newReport",
                            report: battle[current]
                        }));
                        
                        if (current + 1 !== battle.length) {
                            current++;
                            console.log("Onto next")
                        } else {
                            clearInterval(interval);
                            delete interval;
                            ws.close();
                        }
                    }, 1000);

                    ws.on('close', function() {
                        if (interval) {
                            clearInterval(interval);
                            delete interval;
                        }
                    })
                });
            }
        }
    ]
};