var express = require('express');
var generateToken = require('../scripts/generateToken');
var verifyToken = require('../scripts/verifyToken');

module.exports = {
    endpoint: 'user',
    generateRouter: function generateRouter(db) {
        var router = express.Router();
        
        router.get('/:id', (req, res) => {
            db.query('SELECT * FROM `users` WHERE `id` = ?;', [parseInt(req.params.id)], (err, users) => {
                if (err) return res.status(502).json(err);
                if (!users[0]) return res.status(404).json({message: `No user with id ${req.params.id} was found.`})

                delete users[0].password;

                res.json(users[0]);
            });
        });
    
        router.post('/', (req, res) => {
            var user = req.body.user;

            /*for (var key in user) {
                if (typeof user[key] === 'string') {
                    user[key] = db.escape(user[key]); 
                }
            }*/

            console.log(user.email);

            if (!user.email) return res.status(400).json({message: "Email required for account registration."});
            if (!user.display_name) return res.status(400).json({message: "Display name required for account registration."});
            if (!user.password) return res.status(400).json({message: "Password required for account registration."});

            if (user.display_name.length > 20) return res.status(400).json({message: "Display name can't be longer than 20 characters."});
            if (user.password.length < 8) return res.status(400).json({message: "Password must be at least eight characters."});
            if (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(user.email) === false) return res.status(400).json({message: "Email is not properly formatted."});

            // Default values!
            user.stats = JSON.stringify({});
            user.inventory = JSON.stringify([]);
            user.exp = 0;
            user.level = 0;
            user.money = 0;

            user.created_at = Date.now();

            db.query('SELECT * FROM `users` WHERE `email` = ?;', [user.email], (err, rows) => {
                if (err) return res.status(502).json(err);

                if (rows[0]) {
                    return res.status(400).json({
                        message: `Email address ${user.email} is already in use.`
                    });
                } else {
                    db.query('SELECT * FROM `users` WHERE `display_name` = ?;', [user.display_name], (err, rows) => {
                        if (err) return res.status(502).json(err);
        
                        if (rows[0]) {
                            return res.status(400).json({
                                message: `Display name ${user.display_name} is already in use.`
                            });
                        } else {
                            db.query('INSERT INTO `users` SET ?;', [user], (err, data) => {
                                if (err) return res.status(502).json(err);

                                user.id = data.insertId;

                                delete user.password;

                                res.json(user);
                            })
                        }
                    });
                }
            })
        });
        
        router.put('/', (req, res) => {
            res.json({message: 'a'});
        });

        // When a user initializes the front-end, this is called to renew a token.
        router.post('/token', (req, res) => {
            var token = req.body.token;
            var obj = verifyToken(token);

            if (obj || obj.user) { // Handles 24h expiration
                db.query('SELECT * FROM `users` WHERE `token` = ?;', [token], (err, users) => { // Ensures that a user's active session represents a single token
                    if (err) return res.status(502).json(err);

                    // TODO: Ensure multiple tokens aren't present.
                    if (users[0]) {
                        var newToken = generateToken(obj.user);
        
                        db.query('UPDATE `users` SET `token` = "' + newToken + '" WHERE `id` = ' + obj.user + ';', null, (err, data) => {
                            if (err) console.error("[ERROR, POST: /user/token]", err);
        
                            delete users[0].password;

                            users[0].token = newToken;
                            res.json(users[0]);
                        })
                    } else {
                        res.status(403).json({message: "Invalid token, please sign in again."});
                    }
                })
            } else {
                res.status(403).json({message: "Session expired, please sign in again."});
            }
        });

        router.post('/auth', (req, res) => {
            var email = req.body.email;
            var password = req.body.password;

            if (!email || !password) {
                return res.status(400).json({message: "Must specify a email and a password to authenticate."});
            }

            db.query('SELECT * FROM `users` WHERE `email` = ?;', [email], (err, users) => {
                if (err) return res.status(502).json(err);

                if (users[0]) {
                    if (users[0].password === password) {
                        var token = generateToken(users[0].id);
                        
                        db.query('UPDATE `users` SET `token` = "' + token + '" WHERE `id` = ' + users[0].id + ';', null, (err, data) => {
                            if (err) console.error("[ERROR, POST: /user/token]", err);

                            users[0].token = token;
                            delete users[0].password;

                            res.json(users[0]);
                        });
                    }
                } else {
                    res.status(403).json({message: "Incorrect credentials."});
                }
            })
        });
    
        return router;
    }
};