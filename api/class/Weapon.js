module.exports = function Weapon(name, caliber, magCap, bullet) {
    this.name = name || "";
    this.caliber = caliber || "";

    this.parts = [];
    this.magazineCapacity = magCap;
    this.bulletsLeft = magCap;

    this.bullet = bullet;
};