module.exports = function Battle() {
    this.players = [];

    this.battleReport = [];
    this.currentTurn = 0;
 
    this.report = function(obj) {
        this.battleReport.push({
            players: this.players,
            action: obj
        });
    };

    this.shootPlayer = function(shooter, target) {
        var weapon = shooter.weapon;
        var hit = Math.random() * 100 + 1 <= shooter.accuracy; // Is a random number between one and one hundred is less than or equal to the shooter's accuracy?

        if (hit) {
            var targetRange = Math.random() * 100 + 1;
            var limbHit;
            var lastRangeDiff = 100; //Arbitrary number

            //Let's find which limb is closet to our target range.
            target.body.forEach((limb) => {
                var newRangeDiff = Math.abs(limb.hitRange - targetRange); // Distance from target range generated
                
                if (newRangeDiff < lastRangeDiff)  { // Is this one closer than the one we have so far?
                    limbHit = limb;
                    lastRangeDiff = newRangeDiff;
                }
            });

            var bullet = shooter.weapon.bullet;

            var damage = bullet.damage;
            limbHit.health = limbHit.health - damage;
            
            switch(limbHit.name) {
                case 'Head':
                    if (Math.floor(Math.random() * 2) + 1 === 2) {
                        damage = target.health;
                        target.health -= damage;
                        this.report({ damage: damage, summary: target.name + " got domed."});
                    } else {
                        damage = target.health / 2;

                        target.health = target.health / 2;
                        target.accuracy = target.accuracy / 2;

                        this.report({
                            damage: damage,
                            summary: shooter.name + " shot " + target.name + " in the face for "
                                + damage + " damage. " + "As a result, "
                                + target.name + " becomes disoriented and loses half of their accuracy."
                        });
                    }

                    break;
                default:
                    target.health -= damage;
                    this.report({ damage: damage, summary: shooter.name + " shot " + target.name + " in the " + limbHit.name + " for " + damage + " damage."});
                    break;
            }
        } else {
            this.report({ damage: 0, summary: shooter.name + " missed his shot."});
        }
    };

    this.calculateBattle = function() {
        while (this.players[0].health > 0 && this.players[1].health > 0) {
            this.shootPlayer(this.players[0], this.players[1]);

            // Swap their positions. Basically an easy way to determine who's turn it is
            var temp = this.players[0];

            this.players[0] = this.players[1];
            this.players[1] = temp;
        }

        if (this.players[0].health <= 0) {
            this.players[1].victory = true;
            this.report({summary: "The battle is over. " + this.players[0].name + " has fallen."});
        } else {
            this.players[0].victory = true;
            console.log(this.players[0], this.players[1]);
            this.report({summary: "The battle is over. " + this.players[1].name + " has fallen."});
        }

        return this.battleReport;
    };
};