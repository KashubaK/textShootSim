module.exports = function BodyPart(name, health, hitRange) {
    this.name = name || ""; // e.g. "Head"
    this.hitRange = hitRange || 0;
    this.health = health || 100;

    this.effects = [];
}