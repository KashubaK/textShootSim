var Item = require('./Item');

module.exports = function Player(name) {
    this.name = name || "";
    this.health = 700;
    this.accuracy = 45;

    this.body = [];
    this.weapon = {};
    this.inventory = [];
};