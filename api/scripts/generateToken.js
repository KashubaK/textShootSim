var jwt = require('jsonwebtoken');

module.exports = function generateToken(user_id) {
    if (typeof user_id !== 'number') throw "[generateToken] user_id must be a number.";

    var token = jwt.sign(
        { user: user_id, at: Date.now() },
        "jfalskjdf902j34097uiody7fuiah38924y52083y5hq83a74wqh89nd34cx7560q3w7846ncx3w0d",
        { expiresIn: "1d" }
    );

    return token;
}

/*
    According to https://howsecureismypassword.net,
    the secret: "jfalskjdf902j34097uiody7fuiah38924y52083y5hq83a74wqh89nd34cx7560q3w7846ncx3w0d"
    should take 20 trestrigintillion (10^102) years to crack.
*/