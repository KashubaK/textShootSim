var jwt = require('jsonwebtoken');

module.exports = function verifyToken(token) {
    if (typeof token !== "string") throw "[verifyToken] Token provided must be of type string.";

    try {
        var decoded = jwt.verify(
            token, 
            "jfalskjdf902j34097uiody7fuiah38924y52083y5hq83a74wqh89nd34cx7560q3w7846ncx3w0d",
            { maxAge: "1d" }
        );
    } catch (err) {
        return null;
    }
    
    return decoded;
}