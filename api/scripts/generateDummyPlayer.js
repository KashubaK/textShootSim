var BodyPart = require('../class/BodyPart');
var Bullet   = require('../class/Bullet');
var Player   = require('../class/Player');
var Weapon   = require('../class/Weapon');
var Item     = require('../class/Item');

module.exports = function generateDummyPlayer(name) {
    const player = new Player(name);

    player.body = [
        new BodyPart("Head", 100, 5),
        new BodyPart("Chest", 100, 40),
        new BodyPart("Stomach", 100, 60),
        new BodyPart("Left Arm", 100, 70),
        new BodyPart("Right Arm", 100, 80),
        new BodyPart("Left Leg", 100, 90),
        new BodyPart("Right Leg", 100, 100)
    ];

    player.weapon = new Weapon("Makarov", "9x18mm", 8, new Bullet("9x18mm", 30));

    return player;
};